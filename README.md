# MicroFrontEnd

Aplicação focada em testar o Single-spa, uma ferramenta capaz de criar MicroFrontEnds. 

MicroFrontEnd é uma forma de fazer com que a parte de FrontEnd da sua aplicação sejam independentes uma da outra, fazendo assim ela podendo funcionar com frameorks diferentes

Nesta aplicação é utilizada as seguintes ferramentas:

- Yarn: Para a instalação dos pacaotes necessários;
- Single-spa : Principal forma para manter uma arquitetura de MicroFrontEnd;
- React : Principal FrameWork; 
- Material-ui : Blibioteca de funções envolvendo o FrontEnd.
