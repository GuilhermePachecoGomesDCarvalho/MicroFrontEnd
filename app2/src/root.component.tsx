import React, { lazy, Suspense } from "react";
import { createGenerateClassName, StylesProvider  } from "@material-ui/core";
const App2 = lazy(() => import('./App2'));

const generateClassName = createGenerateClassName({
  seed: 'Aplicativo-numero-dois'
});
export default function Root(props) {
  return(
  <StylesProvider generateClassName={generateClassName}>
    <Suspense fallback="loading">
    <App2/>
    </Suspense>
 </StylesProvider>
 );
}