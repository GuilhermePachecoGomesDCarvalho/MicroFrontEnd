import React from "react";
import { Button, Card, CardActions, CardContent, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

const dos = makeStyles({
  e: {
    minWidth: 275,
    margin: 20,
  },
  q: {
    fontSize: 14,
  },
});

export default function App2() {
  const use = dos()
  return ( 
    <Card className={use.e}>
    <CardContent>
      <Typography className={use.q}color='textSecondary'gutterBottom>
        Cima
      </Typography>
    </CardContent>
    <CardActions>
      <Button size='small'>Ver mais</Button>
    </CardActions>
  </Card>
  );
}