import { createGenerateClassName, StylesProvider } from '@material-ui/core';
import { useState } from 'react';
import App3 from './App3';
const generateClassName = createGenerateClassName({
  seed: 'app3'
});
export default function Root(props) {
  return(<StylesProvider generateClassName={generateClassName}>
  <App3 />
</StylesProvider>);
}
