import { makeStyles, Card, CardContent, Typography, CardActions, Button } from "@material-ui/core";
import React from "react";

const uy = makeStyles({
    T: {
      minWidth: 275,
      margin: 20,
    },
    W: {
      fontSize: 14,
    },
  });
  
  export default function App3() {
    const V = uy();
  
    return (
      <Card className={V.T}>
        <CardContent>
          <Typography className={V.W} color='textSecondary'gutterBottom>
            Informações
          </Typography>
        </CardContent>
        <CardActions>
          <Button size='small'>Não há mais informações</Button>
        </CardActions>
      </Card>
    );
  }