import React, { lazy } from "react";
import { createGenerateClassName,StylesProvider  } from "@material-ui/core";
import App from "./App";

const generateClassName = createGenerateClassName({
  seed: 'aplicativo-numero-um'
});
export default function Root(props) {
  return(
  <StylesProvider generateClassName={generateClassName}>
      <App/>
 </StylesProvider>
 );
}

