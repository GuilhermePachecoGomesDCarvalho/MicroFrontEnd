import React from "react";
import { AppBar, Button, Grid, Toolbar, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

const H = makeStyles({
  Y: {
    flexGrow: 1,
  },
  X: {
    flexGrow: 1,
  
},});

export default function App() {
  const uso = H()
  return (
    <div className={uso.Y}>
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" className={uso.X}>
          Curso de MicroFrontend
        </Typography>
      </Toolbar>
    </AppBar>
  </div>
    );
}
