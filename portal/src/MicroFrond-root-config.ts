import { registerApplication, start, LifeCycles, RegisterApplicationConfig } from "single-spa";

const applications: RegisterApplicationConfig[] = [
 {
   name: "@MicroFrond/app1",
   app: () => System.import<LifeCycles>("@MicroFrond/app1"),
   activeWhen: () => true,
   customProps:{
    token: "123456789"
   }
 },{
   name: "@MicroFrond/app2",
   app: () => System.import<LifeCycles>("@MicroFrond/app2"),
   activeWhen: ['/app2'],
   customProps:{
    token: "987654321"
   }
 },{
  name: "@MicroFrond/app3",
  app: () => System.import<LifeCycles>("@MicroFrond/app3"),
  activeWhen: ['/app3']
}];

applications.forEach(registerApplication);

start({
  urlRerouteOnly: true,
});
